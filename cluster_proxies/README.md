# README

Cluster proxy project

## Modeling
### Code
- `cluster_proxy_modeling_final.R`: Fetches data and trains random forest model to produce cluster proxies

- `modeling_data_final.sql`: Query to retrieve training data

- `read_sql.R`: Helper script to read SQL 

### Data

Model data are stored at s3://gp-datascience/proxy_modeling:

- `impute_values.Rdata`: Contains the mean values that were used for imputation of missing values for numeric variables during scoring

- `cluster_proxy_model.Rdata`: Model object, which is a random forest created with the `ranger` package

## Scoring 
Scoring new data will require the above data files

### Code

- `score_state.R`: Example scoring script, which loops over a set of local CSV files containing the necessary predictor variables

- `get_data.R`: Connects to Athena and retrieves data needed for scoring for a single state, in chunks based on first initial of last name

- `read_sql.R`: Reads SQL query into string for use by R

- `get_data_for_scoring.sql`: SQL query to retrieve data

