# get missing IDs for scoring
library(tidyverse)
library(magrittr)
library(aws.s3)
library(DBI)
library(caret)
library(ranger)
#setwd('cluster_proxies/scoring')
source('../read_sql.R')

this_state = 'MI'

## get data

reticulate::use_python('/opt/anaconda3/bin/python')

con <- dbConnect(RAthena::athena(),
                 s3_staging_dir = 's3://gp-client-data/nmf/catalist_ts_match_data_20200310/')


savefile = file.path(this_state, 
                     paste0('input_data_missing.Rdata'))


data_query <- read_sql('get_missing_ids_for_scoring.sql', 
                       parameters = list(state = this_state))

print(savefile)                       

df <- DBI::dbGetQuery(con, data_query)
save(df, file=savefile)



